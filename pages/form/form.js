// pages/form/form.js
var api = require("../../utils/api");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    l: true,
    r: false,
    switch1: false,
    switch2: false,
    switchType: "",
    name: "",
    tax: "",
    address: "",
    tel: "",
    bank: "",
    account: "",
    phone: "",
    email: "",
    uname: "",
    param: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    if (options.id != undefined) {
      let param = JSON.parse(decodeURIComponent(options.id))
      console.log(param);
      this.setData({
        param
      })
    }

    let res = await this.resData(
      "Invoice/headerList",
      {
        userid_locked: wx.getStorageSync("userid_locked")
      }
    )

    console.log(res);
    if (res.result_code == 1000) {
      this.setData({
        name: res.data.company_address,
        tax: res.data.company_tax,
        address: res.data.company_address,
        tel: res.data.company_phone,
        bank: res.data.company_bank,
        account: res.data.company_bank_num,
        phone: res.data.phone,
        email: res.data.email,
        uname: res.data.person_name,
        l: res.data.type == 1 ? true : false,
        r: res.data.type == 2 ? true : false,
        switch1: (res.data.type == 1 && res.data.company_bank_num) || false,
        switch2: (res.data.type == 2 && res.data.company_bank_num) || false
      })
    }
  },

  left: function () {
    this.setData({
      l: true,
      r: false
    });
  },

  right: function () {
    this.setData({
      l: false,
      r: true
    });
  },

  switch1: function (e) {
    console.log(e.detail.value);
    if (e.detail.value) {
      this.setData({
        switch1: true,
        switch2: false,
        switchType: 1
      })
    }
  },

  switch2: function (e) {
    console.log(e.detail.value);
    if (e.detail.value) {
      this.setData({
        switch1: false,
        switch2: true,
        switchType: 2
      })
    }
  },

  submit: async function () {
    const {
      l,
      r,
      param,
      switch1,
      switch2,
      switchType,
      name,
      tax,
      address,
      tel,
      bank,
      account,
      phone,
      email,
      uname } = this.data;
    let emotion = /([^\u0020-\u007E\u00A0-\u00BE\u2E80-\uA4CF\uF900-\uFAFF\uFE30-\uFE4F\uFF00-\uFFEF\u0080-\u009F\u2000-\u201f\u2026\u2022\u20ac\r\n])|(\s)/g
    let content = /[@#$%^&*]+/g
    if (l) {
      if (!name) return wx.showToast({ title: '请输入公司名称', icon: "none" });
      if (!tax) return wx.showToast({ title: '请输入公司税号', icon: "none" });
      if (!address) return wx.showToast({ title: '请输入注册地址', icon: "none" });
      if (!tel) return wx.showToast({ title: '请输入注册电话', icon: "none" });
      if (!bank) return wx.showToast({ title: '请输入开户银行', icon: "none" });
      if (!account) return wx.showToast({ title: '请输入银行账号', icon: "none" });
      if (!phone) return wx.showToast({ title: '请输入联系电话', icon: "none" });
      if (!email) return wx.showToast({ title: '请输入邮箱地址', icon: "none" });
      if (emotion.test(name || tax || address || tel || bank || account || phone || email)) {
        wx.showToast({
          title: '禁止输入表情符/颜文字等非法字符',
          icon: 'none'
        })
      } else if (content.test(name || tax || address || tel || bank || account || phone || email)) {
        wx.showToast({
          title: '输入内容有误',
          icon: 'none'
        })
      } else if (!/^1[3456789]\d{9}$/.test(tel)) {
        wx.showToast({
          title: '注册电话错误',
          icon: 'none'
        })
      } else if (!/^1[3456789]\d{9}$/.test(phone)) {
        wx.showToast({
          title: '电话号错误',
          icon: 'none'
        })
      } else if (!/^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/.test(email)) {
        wx.showToast({
          title: '邮箱错误',
          icon: 'none'
        })
      } else {
        let res = await this.resData(
          "Invoice/headerSubmit",
          {
            ids: param.ids || null,
            userid_locked: wx.getStorageSync("userid_locked"),
            total_num: param.nums,
            total_money: param.fee,
            select_all: param.ids ? 0 : 1,
            type: switchType,
            company_name: name,
            company_tax: tax,
            company_address: address,
            company_phone: tel,
            company_bank: bank,
            company_bank_num: account,
            default: switch1 ? 1 : 0,
            phone,
            email
          }
        );

        console.log(res);

        if (res.result_code == 1000) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          });
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none"
          });
        }
      }
    }

    if (r) {
      if (!uname) return wx.showToast({ title: '请输入姓名', icon: "none" });
      if (emotion.test(uname)) {
        wx.showToast({
          title: '禁止输入表情符/颜文字等非法字符',
          icon: 'none'
        })
      } else if (content.test(uname)) {
        wx.showToast({
          title: '输入内容有误',
          icon: 'none'
        })
      } else {
        let res = await this.resData(
          "Invoice/headerSubmit",
          {
            ids: param.ids || null,
            userid_locked: wx.getStorageSync("userid_locked"),
            total_num: param.nums,
            total_money: param.fee,
            select_all: param.ids ? 0 : 1,
            type: switchType,
            default: switch2 ? 1 : 0,
            person_name: uname
          }
        )

        console.log(res);
        if (res.result_code == 1000) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          });
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none"
          });
        }
      }
    }
  },

  resData: function (url, data) {
    return new Promise((resolve, reject) => {
      api.form_(
        url,
        data,
        undefined,
        function success(res) {
          resolve(res.data)
        },
        function err(err) {
          reject(err)
        }
      );
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})