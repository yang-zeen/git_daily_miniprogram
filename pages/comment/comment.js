// pages/conmment/conmment.js
var api = require('../../utils/api')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message: "",
    time: "",
    profess: "",
    service: "",
    touch: true,
    fileList: [],
    orderId: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      orderId: options.orderId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  times(e) {
    this.setData({
      time: e.detail
    })
  },
  repair(e) {
    this.setData({
      profess: e.detail
    })
  },
  serve(e) {
    this.setData({
      service: e.detail
    })
  },

  afterRead(event) {
    let that = this
    const { file } = event.detail
    wx.uploadFile({
      url: 'https://images.cd1a.cn/appoint_repair.php',
      filePath: file[0].url,
      name: 'file',
      formData: {
        user_id: wx.getStorageSync('userid_locked')
      },
      success(res) {
        let result = JSON.parse(res.data)
        console.log(result)
        if (result.result_code == "205") {
          that.data.fileList.push({ url: result.data[0] })
          that.setData({
            fileList: that.data.fileList
          })
        } else {
          wx.showToast({
            title: result.msg,
            icon: 'none'
          })
        }
      },
      fail(err) {
        wx.showToast({
          title: err.msg,
          icon: 'none'
        })
      }
    })
  },

  commit() {
    let arrImg = []
    if (this.data.time == 0) {
      wx.showToast({
        title: '请对上门做出评价!',
        icon: 'none'
      })
    } else if (this.data.profess == 0) {
      wx.showToast({
        title: '请对专业做出评价!',
        icon: 'none'
      })
    } else if (this.data.service == 0) {
      wx.showToast({
        title: '请对服务做出评价!',
        icon: 'none'
      })
    } else if (this.data.fileList.length == 0) {
      wx.showToast({
        title: "请上传图片!",
        icon: 'none'
      })
    } else {
      if (this.regRule(this.data.message)) {
        wx.showToast({
          title: this.regRule(this.data.message),
          icon: 'none'
        })
      } else {
        if (this.data.touch) {
          this.setData({
            touch: false
          })

          this.data.fileList.forEach(item => {
            arrImg.push(item.url)
          })

          api.form_('Serve_car/estimate', {
            userid_locked: wx.getStorageSync('userid_locked'),
            appoint_id: this.data.orderId,
            on_time: this.data.time * 2,
            major: this.data.profess * 2,
            serve: this.data.service * 2,
            img: JSON.stringify(arrImg),
            remark: this.data.message,
          },
          null,
          function success(res) {
            console.log(res.data)
            if (res.data.result_code == "308") {
              wx.showToast({
                title: res.data.msg,
                icon: "none"
              })
              setTimeout(() => {
                wx.reLaunch({
                  url: '../home/home',
                })
              }, 1000)
            } else {
              wx.showToast({
                title: res.data.msg,
                icon: "none"
              })
            }
          },
          function fail(err) {
            console.log(err)
          }
          )
          setTimeout(() => {
            this.setData({
              touch: true
            })
          }, 3000)
        } else {
          wx.showToast({
            title: '您点击的次数太快了',
          })
        }
      }
    }
  },

  deleteImg(e) {
    let arrImg = this.data.fileList.filter((item, index) => index != e.detail.index)
    this.setData({
      fileList: arrImg
    })
  },

  regRule(param) {
    let msg
    let emotion = /([^\u0020-\u007E\u00A0-\u00BE\u2E80-\uA4CF\uF900-\uFAFF\uFE30-\uFE4F\uFF00-\uFFEF\u0080-\u009F\u2000-\u201f\u2026\u2022\u20ac\r\n])|(\s)/g
    let content = /[@#$%^&*]+/g
    if (emotion.test(param)) {
      return msg = '禁止输入表情符/颜文字等非法字符'
    } else if (content.test(param)) {
      return msg = '输入内容有误'
    } else if (!param) {
      return msg = '内容不能为空'
    } else {
      return msg = ''
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})