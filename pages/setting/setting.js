// pages/setting/setting.js
var api = require("../../utils/api");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    set: [
      {
        title: "账号与个人信息管理",
        url: "../charging/charging"
      }
    ]
  },

  submit: function () {
    api.form_(
      'Login/wxAppletLogout',
      {
        userid_locked: wx.getStorageSync("userid_locked")
      },
      undefined,
      function(res) {
        console.log(res);
        if (res.data.result_code == 1000) {
          wx.reLaunch({
            url: '../index/index'
          });

          return;
        };

        wx.showToast({
          title: res.data.msg,
          icon: "none"
        });
      }
    );
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  navigate: function(e) {
    let index = e.target.dataset.index;
    wx.navigateTo({
      url: this.data.set[index].url,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})