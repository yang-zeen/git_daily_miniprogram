var api = require('../../utils/api.js')
var timer, http
Page({

  /**
   * 页面的初始数据
   */
  data: {
    progress_date: '00:00:00',
    nowDate: 0,
    progress_startTime: 1, // 不要设置为0 在特定的情况会if跳出 开始时间
    progress_endTime: 1, // 结束时间
    all_time: '', // 总时长
    canvasWidth: 0,
    log_id: '',
    olog_id: '',
    refreshTime: 140000, //120 000ms 2min
    repairOutMin: 30, //报修有效分钟数
    mask: true,
    get_coupon: '',
    coupon: '',
    takeTime: 5,
    plug_remind: '',
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0,
    full_stop: '',
    progress_times: '',
    localTime: '',
    residue_timestamp: '',
    data: '',
    carousel_first: [],
    carousel_two: [],
    img: '',
    type: '',
    url: '',
    url_platform: '',
    car_shop_appid: '',
    singleAngle: 45, // 每片扇形的角度
    activity_data: '',
    three_hour: '1',
    one_hour: '5',
    year_card: '3',
    msg: '',
    is_corona: '',
    gift_id: '',
    is_start: true,
    animationData: {},
    open_tell: '',
    activity: false,
    account: '',
    scanCode: '',
    hide: false,
    close: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    if (!wx.getStorageSync('scanCode')) {
      this.setData({
        scanCode: 1
      })
    }

    // console.log("coupon类型", options.get_coupon);

    this.setData({
      olog_id: options.log_id,
      coupon: options.get_coupon,
      open_tell: wx.getStorageSync('open_tell')
    })

    this.animation = wx.createAnimation({
      delay: 0,
      duration: 3000,
      timingFunction: 'ease'
    })

    this.loadData(); // 调用时间接口
  },

  onShow: function () {
    if (this.data.hide) {
      this.loadData()
      this.countInterval()
    }
    console.log(this.data.hide)
  },

  // 调用倒计时背景
  backgroundCircle: function () {
    let that = this;
    wx.createSelectorQuery().select('.progress_box').boundingClientRect(function (rect) {
      if (!rect) return;
      var width = rect.width / 2 // 节点的宽度
      that.setData({
        canvasWidth: width
      })
      that.drawProgressbg()
    }).exec()
  },

  // 关注公众号
  // count: function () {
  //   let that = this
  //   console.log(that.data.type);
  //   if (that.data.type == 1) {
  //     wx.navigateToMiniProgram({
  //       appId: that.data.car_shop_appid,
  //       path: that.data.url_platform,
  //       success: function (res) {
  //         let data = {
  //           userid_locked: wx.getStorageSync('userid_locked'),
  //           url: that.data.url_platform
  //         }
  //         api.form_('Statist/out_applet', data, null, function success(res) {
  //           console.log(res)
  //         })
  //       },
  //       extraData: {
  //         foo: 'bar'
  //       },
  //       envVersion: 'release'
  //     })
  //   } else if (that.data.type == 2) {
  //     wx.navigateTo({
  //       url: '../battery/battery?url=' + that.data.url
  //     })
  //   } else if (that.data.type == 6) {
  //     this.setData({
  //       close: true
  //     })
  //     wx.navigateTo({
  //       url: '../count/count?url=' + this.data.account,
  //     }) 
  //   }
  // },

  // 抽奖
  rotateAni: function () {
    let that = this
    let tmpnum = 0.9
    let angle = 2 * 360
    if (that.data.gift_id % 2 == 0) {
      let endAddAngle = tmpnum * that.data.singleAngle + angle // 中奖角度
      that.animation.rotate(endAddAngle).step()
      that.setData({
        animationData: that.animation.export()
      })
      if (!that.data.is_start) {
        setTimeout(() => {
          wx.showModal({
            title: '中奖结果',
            content: that.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.setData({
                  is_corona: 1,
                  coupon: 1,
                  activity: false
                })
                setTimeout(() => {
                  that.backgroundCircle()
                  that.countInterval()
                }, 500)
              }
            }
          })
        }, 4000)
      }
    } else if (that.data.gift_id == that.data.three_hour) {
      let endAddAngle = tmpnum * that.data.singleAngle - parseInt(tmpnum * that.data.singleAngle) + angle // 中奖角度
      that.animation.rotate(endAddAngle).step()
      that.setData({
        animationData: that.animation.export()
      })
      if (!that.data.is_start) {
        setTimeout(() => {
          wx.showModal({
            title: '中奖结果',
            content: that.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.setData({
                  is_corona: 1,
                  coupon: 1,
                  activity: false
                })
                setTimeout(() => {
                  that.backgroundCircle()
                  that.countInterval()
                }, 500)
              }
            }
          })
        }, 4000)
      }
    } else if (that.data.gift_id == that.data.year_card) {
      let endAddAngle = tmpnum * that.data.singleAngle - parseInt(tmpnum * that.data.singleAngle) + 90 + angle // 中奖角度
      that.animation.rotate(endAddAngle).step()
      that.setData({
        animationData: that.animation.export()
      })
      if (!that.data.is_start) {
        setTimeout(() => {
          wx.showModal({
            title: '中奖结果',
            content: that.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.setData({
                  is_corona: 1,
                  coupon: 1,
                  activity: false
                })
                setTimeout(() => {
                  that.backgroundCircle()
                  that.countInterval()
                }, 500)
              }
            }
          })
        }, 4000)
      }
    } else if (that.data.gift_id == that.data.one_hour) {
      let endAddAngle = tmpnum * that.data.singleAngle - parseInt(tmpnum * that.data.singleAngle) + 180 + angle // 中奖角度
      that.animation.rotate(endAddAngle).step()
      that.setData({
        animationData: that.animation.export()
      })
      if (!that.data.is_start) {
        setTimeout(() => {
          wx.showModal({
            title: '中奖结果',
            content: that.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.setData({
                  is_corona: 1,
                  coupon: 1,
                  activity: false
                })
                setTimeout(() => {
                  that.backgroundCircle()
                  that.countInterval()
                }, 500)
              }
            }
          })
        }, 4000)
      }
    } else {
      let endAddAngle = tmpnum * that.data.singleAngle - parseInt(tmpnum * that.data.singleAngle) + 315 + angle // 中奖角度
      that.animation.rotate(endAddAngle).step()
      that.setData({
        animationData: that.animation.export()
      })
      if (!that.data.is_start) {
        setTimeout(() => {
          wx.showModal({
            title: '中奖结果',
            content: that.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.setData({
                  is_corona: 1,
                  coupon: 1,
                  activity: false
                })
                setTimeout(() => {
                  that.backgroundCircle()
                  that.countInterval()
                }, 500)
              }
            }
          })
        }, 4000)
      }
    }
  },

  // 抽奖
  start() {
    let that = this
    let data = {
      userid_locked: wx.getStorageSync("userid_locked"),
      log_id: that.data.log_id,
      gift_id: that.data.gift_id
    }
    if (that.data.is_start) {
      that.setData({
        is_start: false
      })
      if (that.data.activity_data.type == '1') {
        that.rotateAni()
      } else {
        api.form_('Activity/corona_backfeed', data, null, function success(res) {
          console.log(res.data)
          if (res.data.result_code == 300) {
            that.rotateAni()
          } else {
            wx.showToast({
              title: res.data.msg,
              icon: 'none'
            })
          }
        })
      }
    }
  },

  get: function () {
    this.setData({
      mask: false
    })
  },

  jd: function () {
    wx.navigateToMiniProgram({
      appId: 'wx91d27dbf599dff74',
      path: '/pages/union/proxy/proxy?spreadUrl=https%3A%2F%2Fu.jd.com%2FpwSnPAp&EA_PTAG=jingdonghuimin',
      extraData: {
        foo: 'bar'
      },
      envVersion: 'release'
    })
  },

  // 第一个轮播
  first: function (e) {
    let index = e.currentTarget.dataset.index
    let that = this
    if (this.data.carousel_first[index].type == 1) {
      wx.navigateToMiniProgram({
        appId: this.data.carousel_first[index].car_shop_appid,
        path: this.data.carousel_first[index].url_platform,
        success: function (res) {
          let data = {
            userid_locked: wx.getStorageSync('userid_locked'),
            url: that.data.carousel_first[index].url_platform
          }
          api.form_('Statist/out_applet', data, null, function success(res) {
            console.log(res)
          })
        },
        extraData: {
          foo: 'bar'
        },
        envVersion: 'release'
      })
    } else if (this.data.carousel_first[index].type == 2) {
      let reg = this.data.carousel_first[index].url.search("appKey");
      if (reg == -1) {
        wx.navigateTo({
          url: '../battery/battery?url=' + this.data.carousel_first[index].url
        })
      } else {
        wx.setStorageSync('appKey', this.data.carousel_first[index].url)
        wx.navigateTo({
          url: '../activity/activity'
        })
      }
    } else if (this.data.carousel_first[index].type == 3) {
      wx.navigateTo({
        url: '../integralSharing/integralSharing'
      })
    } else if (this.data.carousel_first[index].type == 4) {
      wx.navigateTo({
        url: '/pages/buyCar/invitation/invitation'
      })
    } else if (this.data.carousel_first[index].type == 6) {
      wx.navigateTo({
        url: '../count/count?url=' + this.data.account
      })
    }
  },

  // 第二个轮播
  two: function (e) {
    let index = e.currentTarget.dataset.index
    let that = this
    if (this.data.carousel_two[index].type == 1) {
      wx.navigateToMiniProgram({
        appId: that.data.carousel_two[index].car_shop_appid,
        path: that.data.carousel_two[index].url_platform,
        success: function (res) {
          let data = {
            userid_locked: wx.getStorageSync('userid_locked'),
            url: that.data.carousel_two[index].url_platform
          }
          api.form_('Statist/out_applet', data, null, function success(res) {
            console.log(res)
          })
        },
        extraData: {
          foo: 'bar'
        },
        envVersion: 'release'
      })
    } else if (this.data.carousel_two[index].type == 2) {
      let reg = this.data.carousel_two[index].url.search("appKey");
      if (reg == -1) {
        wx.navigateTo({
          url: '../battery/battery?url=' + this.data.carousel_two[index].url
        })
      } else {
        wx.setStorageSync('appKey', this.data.carousel_two[index].url)
        wx.navigateTo({
          url: '../activity/activity'
        })
      }
    } else if (this.data.carousel_two[index].type == 3) {
      wx.navigateTo({
        url: '../integralSharing/integralSharing'
      })
    } else if (this.data.carousel_two[index].type == 4) {
      wx.navigateTo({
        url: '/pages/buyCar/invitation/invitation'
      })
    }
  },

  // js本地时间获取
  localTime: function () {
    let date = new Date()
    let local_start = Math.round(date.getTime() / 1000) // 毫秒
    return local_start
  },

  loadData: function (options) {
    let that = this
    console.log(that.data.activity);
    var data = {
      userid_locked: wx.getStorageSync("userid_locked"),
      user_log_id: that.data.olog_id,
      coupon: that.data.coupon,
      scan_code: wx.getStorageSync('scanCode') ? wx.getStorageSync('scanCode') : that.data.scanCode
    }

    api.form_('Scan/countdown', data, null, function success(res) {
      that.setData({
        localTime: that.localTime(), // 本地时间
        account: res.data.data.is_focus_url
      })
      console.log(res.data.data)
      if (res.data.data.is_focus == '2' && wx.getStorageSync('scanCode')) {
        setTimeout(() => {
          wx.navigateTo({
            url: '../count/count?url=' + res.data.data.is_focus_url
          })
        }, 1500)
      }
      if (res.data.data.pop_up.length != 0) {
        if (!that.data.close) {
          if (!that.data.hide) {
            res.data.data.pop_up.forEach(item => {
              that.setData({
                img: item.img,
                type: item.type,
                url: item.url,
                url_platform: item.url_platform,
                car_shop_appid: item.car_shop_appid
              })
            })
          }
        }
      } else {
        if (res.data.data.is_corona == 2)
        that.setData({
          activity: true
        })
      }
      res.data.data.carousel_first.sort((a, b) => {
        return a.type - b.type
      })
      res.data.data.carousel_two.sort((a, b) => {
        return a.type - b.type
      })
      var timee = res.data.data.all_timestamp // 总时间
      var times = (res.data.data.all_timestamp - res.data.data.residue_timestamp) // 总时间 - 剩余时间 = 开始时间
      // console.log(times);
      that.setData({
        data: res.data,
        gift_id: res.data.data.corona.length > 0 ? res.data.data.corona[0].gift_id : '', // 中奖下标
        msg: res.data.data.corona.length > 0 ? res.data.data.corona[0].gift_name : '', // 中奖信息
        activity_data: res.data.data.corona.length > 0 ? res.data.data.corona[0] : '',
        is_corona: res.data.data.is_corona,
        all_time: res.data.data.all_timestamp,
        residue_timestamp: res.data.data.residue_timestamp,
        progress_startTime: times,
        progress_endTime: timee,
        log_id: res.data.data.log_id,
        get_coupon: res.data.data.get_coupon,
        plug_remind: res.data.data.plug_remind,
        full_stop: res.data.data.full_stop,
        carousel_first: res.data.data.carousel_first,
        carousel_two: res.data.data.carousel_two
      })

      if (res.data.data.is_corona != 2) {
        that.backgroundCircle()
        that.countInterval()
      }

      clearInterval(http)
      http = setInterval(() => {
        that.loadData()
      }, 60000)

      that.takeTimeOut()
    }, function fail(e) {
      console.log(e)
    })
  },

  shop_mall: function () {
    if (this.data.type == 1) {
      wx.navigateToMiniProgram({
        appId: this.data.car_shop_appid,
        path: this.data.url_platform,
        extraData: {
          foo: 'bar'
        },
        envVersion: 'release'
      })
    } else if (this.data.type == 2) {
      const reg = this.data.url.search("appKey");
      if (reg == -1) {
        wx.navigateTo({
          url: '../battery/battery?url=' + this.data.url
        })
      } else {
        wx.setStorageSync('appKey', this.data.url)
        wx.navigateTo({
          url: '../activity/activity'
        })
      }
    } else if (this.data.type == 3) {
      wx.navigateTo({
        url: '../integralSharing/integralSharing'
      })
    } else if (this.data.type == 4) {
      wx.navigateTo({
        url: '/pages/buyCar/invitation/invitation'
      })
    } else if (this.data.type == 6) {
      this.setData({
        close: true
      })
      wx.navigateTo({
        url: '../count/count?url=' + this.data.account,
      }) 
    }
  },

  countInterval: function () {
    if (this.data.full_stop == 2) {
      clearInterval(timer)
      timer = setInterval(() => {
        let times = this.localTime() // 调用本地时间
        let startTimes = this.data.localTime // 第一次记录本地时间
        let nowDate = this.data.progress_startTime + (times - startTimes) // 后台开始时间 + 本地时间 = 实际走过的时间
        let realTime = this.timeFormat(nowDate)
        this.drawCircle(nowDate)
        this.setData({
          progress_date: realTime,
          nowDate: nowDate
        })
        if (nowDate / 1 >= this.data.all_time / 1) {
          wx.reLaunch({
            url: '../index/index'
          })
        }
      }, 1000)
    } else {
      // 设置倒计时 定时器 每500毫秒执行一次 ,耗时2秒绘一圈
      clearInterval(timer)
      timer = setInterval(() => {
        let times = this.localTime() // 调用本地时间
        let startTimes = this.data.localTime // 第一次记录本地时间
        let nowDate = this.data.residue_timestamp - (times - startTimes) // 后台剩余时间 - 本地走过的时间 = 实际剩余的时间
        let realTime = this.timeFormat(nowDate)
        this.drawCircle((this.data.progress_endTime - nowDate) / this.data.progress_endTime * 2) // progress_endTime 总时间
        this.setData({
          progress_date: realTime,
          nowDate: nowDate
        })
        if (nowDate <= 0) {
          wx.reLaunch({
            url: '../index/index'
          })
        }
      }, 1000)
    }
  },

  timeFormat: function (time) {
    //shijianchuo是整数，否则要parseInt转换
    var s = Math.floor(time % 60);
    var h = Math.floor(time / 3600);
    var m = Math.floor(Math.floor(time / 60) - Math.floor(h * 60));
    return this.add0(h) + ':' + this.add0(m) + ':' + this.add0(s);
  },

  //倒计时 （5s倒计时）
  takeTimeOut: function () {
    var that = this;
    setTimeout(function () {
      var takeTime = that.data.takeTime - 1;
      that.setData({
        takeTime: takeTime
      })
      if (takeTime > 0) {
        that.takeTimeOut();
      }
    }, 1000);
  },

  // 关闭
  close: function () {
    if (this.data.takeTime == 0) {
      this.setData({
        mask: false
      })
    }
    if (this.data.is_corona == 2) {
      this.setData({
        activity: true
      })
    }
  },

  show_close: function () {
    this.setData({
      is_corona: 1,
      activity: false
    })
    setTimeout(() => {
      this.backgroundCircle()
      this.countInterval()
    }, 500)
  },

  off: function () {
    if (this.data.is_corona == 2) {
      this.setData({
        activity: true
      })
    }
    this.setData({
      mask: false
    })
  },

  onReady: function () {

  },

  // 圆圈背景
  drawProgressbg: function (a) {
    let that = this
    wx.createSelectorQuery().select('.progress_bg').context(function (res) {
      var ctx = res.context
      ctx.setLineWidth((that.data.canvasWidth / 120) * 4) // 设置圆环的宽度
      ctx.setStrokeStyle('#E0E0E0') // 设置圆环的颜色 灰色
      ctx.setLineCap('round') // 设置圆环端点的形状
      ctx.beginPath() //开始一个新的路径
      ctx.arc(
        that.data.canvasWidth,
        that.data.canvasWidth,
        that.data.canvasWidth - ((that.data.canvasWidth / 120) * 14),
        0,
        Math.PI * 2,
        false
      );
      ctx.stroke() //对当前路径进行描边
      ctx.draw()
    }).exec()
  },

  // 圆圈颜色
  drawCircle: function (step) {
    let that = this
    wx.createSelectorQuery().select('.progress_canvas').context(function (res) {
      var context = res.context
      context.setLineWidth((that.data.canvasWidth / 120) * 4)
      context.setStrokeStyle("#0DA297")
      context.setLineCap('round')
      context.beginPath()
      //添加阴影
      context.shadowBlur = (that.data.canvasWidth / 120) * 12
      context.shadowColor = "#0DA297"
      context.arc(
        that.data.canvasWidth,
        that.data.canvasWidth,
        that.data.canvasWidth - ((that.data.canvasWidth / 120) * 14), -Math.PI / 2,
        step * Math.PI - Math.PI / 2,
        false
      )
      context.stroke()
      context.draw()
    }).exec()
  },

  add0: function (m) {
    return m < 10 ? '0' + m : m
  },

  align: function () {
    wx.reLaunch({
      url: '../index/index'
    });
  },


  onConfirm: function () {
    let that = this
    that.setData({
      showModal: false
    })
    api.form_('Scan/stop_charge', {
        userid_locked: wx.getStorageSync("userid_locked"),
        charge_id: that.data.olog_id
      },
      null,
      function success(res) {
        console.log(res)
        if (!res.data) {
          wx.showToast({
            title: '请勿重复操作！',
            icon: 'none'
          })
        } else {
          if (res.data.return_code == '200') {
            wx.showToast({
              title: res.data.msg,
              icon: 'loading',
              success: function () {
                wx.reLaunch({
                  url: '../index/index',
                })
              }
            })
          }
        }
      }
    )
  },

  stop: function () {
    let that = this
    if (that.data.full_stop == 2) {
      api.form_('Agreement/charge_full_agreement', {
        "userid_locked": wx.getStorageSync('userid_locked')
      }, null, function success(res) {
        wx.showModal({
          title: '温馨提示',
          content: res.data.data.stop_tips,
          success: function (res) {
            if (res.confirm) {
              that.onConfirm()
            }
          }
        })
      })
    } else {
      wx.showModal({
        title: '充电不满1小时按1小时收费,确定停充?',
        success: function (res) {
          if (res.confirm) {
            that.onConfirm()
          }
        }
      })
    }
  },

  repair: function () {
    let that = this;
    if (that.data.progress_startTime / 1000 / 60 > that.data.repairOutMin) {
      wx.showToast({
        title: '时间超过' + that.data.repairOutMin + '分钟不能报修',
        icon: 'none',
        duration: 4000,
        mask: true
      })
      return;
    }
    wx.navigateTo({
      url: '../repair/repair?log_id=' + this.data.log_id
    })
  },

  onUnload: function () {
    this.setData({
      hide: false
    })
    clearInterval(http)
    clearInterval(timer)
    wx.removeStorageSync('scanCode')
  },

  onHide: function () {
    this.setData({
      hide: true
    })
    console.log(this.data.hide)
    clearInterval(http)
    clearInterval(timer)
    wx.removeStorageSync('scanCode')
  }
})