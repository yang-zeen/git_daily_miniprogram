// pages/order/order.js
var api = require('../../utils/api')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    enough: false,
    height: '',
    list: [
      "",
      {
        name: "申请退款",
        url: '../payment/payment'
      },
      {
        name: "联系维修工",
      },
      {
        name: "查看维修方案",
        url: "../battery/battery?url=https://api.cd1a.cn/activity/vue/index.html#/Remethod"
      },
      {
        name: "支付尾款",
        url: "../payment/payment"
      },
      {
        name: "去评价",
        url: "../comment/comment"
      },
    ],
    resData: [],
    resOrder: [],
    page: 1,
    newResult: null,
    height: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    wx.getSystemInfo({
      success (res) {
        that.setData({
          height: res.windowHeight - 100
        })
      }
    })
  },

  loadMore() {
    if (!this.data.enough) {
      this.setData({
        page: this.data.page + 1
      })

      this.https('Serve_car/history', {
        userid_locked: wx.getStorageSync('userid_locked'),
        page: this.data.page
      }).then(res => {
        console.log(res.data)
        if (this.data.resData.length >= res.data.list_num) {
          this.setData({
            enough: true
          })
        } else {
          let startsArr = res.data.list.map(item => {
            return { stars: Number(item.evaluate) }
          })
          let newStartArr = startsArr.map((item, index) => {
            return { ...item, ...res.data.list[index] }
          })
          this.data.resData.push(...newStartArr)
          this.setData({
            resData: this.data.resData
          })
        }
      })
    }
  },

  jump(e) {
    let index = e.currentTarget.dataset.index
    if (this.data.resOrder[index].type == "2") {
      if (this.data.newResult.status == "2") {
        wx.navigateTo({
          url: this.data.resOrder[index].url + '?orderId=' + this.data.newResult.appoint_id + '&refund=111'
        })
      } else {
        if (this.data.resOrder[index].type == "7") {
          wx.showToast({
            title: '暂不可以退款',
            icon: 'none',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '您已提交退款申请，请耐心等候!',
            icon: 'none',
            duration: 2000
          })
        }
      }
    } else if (this.data.resOrder[index].type == "3") {
      wx.makePhoneCall({
        phoneNumber: this.data.newResult.phone
      })
    } else if (this.data.resOrder[index].type == "4") {
      if (this.data.newResult.task_node == "1") {
        wx.navigateTo({
          url: this.data.resOrder[index].url + '&key=' + this.data.newResult.appoint_id
        })
      } else {
        wx.navigateTo({
          url: this.data.resOrder[index].url + '&key=' + this.data.newResult.appoint_id + '&hd=111'
        })
      }
    } else {
      wx.navigateTo({
        url: this.data.resOrder[index].url + '?orderId=' + this.data.newResult.appoint_id
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  https(url, param) {
    return new Promise((resolve, reject) => {
      api.form_(url,
        param,
        null,
        success => {
          resolve(success.data)
        },
        fail => {
          reject(fail.data)
        })
    })
  },

  call(e) {
    wx.makePhoneCall({
      phoneNumber: this.data.resData[e.currentTarget.dataset.index].phone
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let newArr
    Promise.all([
      this.https('Serve_car/doing', {
        userid_locked: wx.getStorageSync('userid_locked'),
      }),
      this.https('Serve_car/history', {
        userid_locked: wx.getStorageSync('userid_locked'),
        page: this.data.page
      })
    ])
    .then(res => {
      console.log(res)
      const [ doing, history ] = res
      if (doing.data.length != '0') {
        if (doing.data.type == "1") {
          if (doing.data.status == "1") {
            let arr = this.data.list.filter((item, index) => index != 1)
            arr.unshift("")
            if (doing.data.show_end == "1") {
              let newArr = arr.filter((item, index) => index != this.data.list.length - 2)
              newArr.splice(newArr.length - 1, 0, "")
              console.log(newArr)
              newArr = doing.data.list.map((item, index) => {
                return { ...item, ...newArr[index] }
              })
              delete doing.data.list
              this.setData({
                newResult: doing.data,
                resOrder: newArr
              })
              return
            }
            newArr = doing.data.list.map((item, index) => {
              return { ...item, ...arr[index] }
            })
          } else {
            newArr = doing.data.list.map((item, index) => {
              return { ...item, ...this.data.list[index] }
            })
          }
        } else {
          let arr = this.data.list.filter((item, index) => index != 1)
          if (doing.data.show_end == "1") {
            let newArr = arr.filter((item, index) => index != 3)
            newArr.splice(3, 0, "")
            newArr = doing.data.list.map((item, index) => {
              return { ...item, ...newArr[index] }
            })
            delete doing.data.list
            this.setData({
              newResult: doing.data,
              resOrder: newArr
            })
            return
          }

          newArr = doing.data.list.map((item, index) => {
            return { ...item, ...arr[index] }
          })
        }
        
        delete doing.data.list
        this.setData({
          newResult: doing.data,
          resOrder: newArr
        })
      }

      if (history.data.list.length != '0') {
        let arr = []
        let startsArr = history.data.list.map(item => {
          return { stars: Number(item.evaluate) }
        })

        let newStartArr = startsArr.map((item, index) => {
          return { ...item, ...history.data.list[index] }
        })
        
        arr.push(...newStartArr)
        this.setData({
          resData: arr
        })
        if (history.data.list.length >= history.data.list_num) {
          this.setData({
            enough: true
          })
        } 
      } else {
        this.setData({
          enough: true
        })
      }
    })
    .catch(err => console.log(err))
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})