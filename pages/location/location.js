var QQMapWX = require('../../plugins/qqmap-wx-jssdk')
var qqmapsdk = new QQMapWX({
  key: '5WYBZ-NFMAK-G7OJL-AENBM-RC7XH-GPBGG'
})
var api = require('../../utils/api')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    radio: "0",
    name: "",
    digit: "",
    userMsg: "",
    phoneMsg: "",
    addressMsg: "",
    info: "",
    multiArray: [
      ["陕西省"],
      ["西安市"],
      [
        "莲湖区",
        "碑林区",
        "新城区",
        "雁塔区",
        "灞桥区",
        "未央区",
        "阎良区",
        "临潼区",
        "长安区",
        "高陵区",
        "鄠邑区"
      ]
    ],
    location: "",
    region: "",
    choose: false,
    column: "",
    value: "",
    latitude: "",
    longitude: "",
    address_id: 0,
    area: "",
    times: true,
    zone: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let that = this

    if (options.address_id) {
      let params = JSON.parse(decodeURIComponent(options.address_id))
      console.log(params)
      that.setData({
        address_id: params.address_id,
        info: params.address,
        name: params.consignee,
        digit: params.phone,
        radio: params.sex,
        area: params.area,
        region: params.province + params.city + params.area
      })
    }

    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success(res) {
        qqmapsdk.reverseGeocoder({
          location: {
            latitude: res.latitude,
            longitude: res.longitude
          },
          success: function (addressRes) {
            // console.log(addressRes.result.address)
            that.setData({
              location: addressRes.result.address,
              latitude: res.latitude,
              longitude: res.longitude
            })
          }
        })
      },
      fail: function (err) {
        wx.getSetting({
          success: function (res) {
            console.log(res) //可以判断用户是否 取消授权了 以便后续可以再次提醒他授权
          }
        })
      }
    })
  },

  // 点击地图
  map: function () {
    let that = this
    //小程序的手动选择位置
    wx.chooseLocation({
      // 确定
      success: function (res) {
        console.log(res)
        that.setData({
          location: res.name,
          latitude: res.latitude,
          longitude: res.longitude
        })
      },
      // 取消
      fail: function (err) {
        console.log(err)
        wx.showModal({
          title: '温馨提示',
          content: '无法获取当前位置,请重试!',
          showCancel: false
        })
      }
    })
  },

  // 姓名
  uname: function (e) {
    this.setData({
      userMsg: this.regRule(e.detail)
    })
    
    if (!this.regRule(e.detail)) {
      this.setData({
        userMsg: ""
      })
    }

    this.setData({
      name: e.detail
    })
  },

  // 性别
  change: function (e) {
    this.setData({
      radio: e.detail
    })
  },

  // 电话
  uphone: function (e) {
    if (!e.detail) {
      this.setData({
        phoneMsg: "内容不能为空",
        digit: e.detail
      })
    } else if (!/^1[3456789]\d{9}$/.test(e.detail)) {
      this.setData({
        phoneMsg: "电话有误请重新输入",
        digit: e.detail
      })
    } else {
      this.setData({
        digit: e.detail,
        phoneMsg: ''
      })
    }
  },

  // 地区
  bindRegionChange: function (e) {
    if (this.data.choose) {
      this.setData({
        region: this.data.multiArray[0][0] + this.data.multiArray[1][0] + this.data.multiArray[this.data.column][this.data.value],
        choose: false,
        zone: true
      })
      console.log(this.data.region)
    } else {
      this.setData({
        region: this.data.multiArray[0][0] + this.data.multiArray[1][0] + this.data.multiArray[2][0],
        column: 2,
        value: 0,
        zone: true
      })
      console.log(this.data.region)
    }
  },
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    this.setData({
      choose: true,
      column: e.detail.column,
      value: e.detail.value
    })
  },

  // 详细地址
  add: function (e) {
    this.setData({
      addressMsg: this.regRule(e.detail),
      info: e.detail
    })
    if (!this.regRule(e.detail)) {
      this.setData({
        addressMsg: ""
      })
    }
  },

  // 提交
  submit: function () {
    if (this.regRule(this.data.name)) {
      wx.showToast({
        title: this.regRule(this.data.name),
        icon: 'none'
      })
    } else if (this.regRule(this.data.digit)) {
      wx.showToast({
        title: this.regRule(this.data.digit),
        icon: 'none'
      })
    } else if (this.regRule(this.data.region)) {
      wx.showToast({
        title: this.regRule(this.data.region),
        icon: 'none'
      })
    } else if (this.regRule(this.data.info)) {
      wx.showToast({
        title: this.regRule(this.data.info),
        icon: 'none'
      })
    } else {
      if (this.data.times) {
        this.setData({
          times: false
        })

        api.form_('Serve_car/add_address', {
            userid_locked: wx.getStorageSync('userid_locked'),
            address_id: this.data.address_id, // 地址id
            consignee: this.data.name, // 联系人
            phone: this.data.digit, // 电话
            province: this.data.multiArray[0][0], // 省
            city: this.data.multiArray[1][0], // 市
            area:  this.data.zone ? this.data.multiArray[this.data.column][this.data.value] : this.data.area, // 区
            address: this.data.info, // 详细地址
            sex: this.data.radio, // 性别
            label: 5,
            defaults: 1,
            latitude: this.data.latitude,
            longitude: this.data.longitude,
            community_name: this.data.location, // 所在地
          },
          null,
          function success_(res) {
            console.log(res.data)
            // 成功
            if (res.data.result_code == '308') {
              wx.showToast({
                title: res.data.msg,
              })
              setTimeout(() => {
                wx.navigateBack({
                  delta: 1,
                })
              }, 1000)
            } else {
              // 失败
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              })
            }
          }
        )
        setTimeout(() => {
          this.setData({
            times: true
          })
        }, 4000)
      } else {
        wx.showToast({
          title: '您点击次数太快!',
          icon: 'none'
        })
      }
    }
  },

  // 表单验证
  regRule: function (param) {
    let msg = ''
    let emotion = /([^\u0020-\u007E\u00A0-\u00BE\u2E80-\uA4CF\uF900-\uFAFF\uFE30-\uFE4F\uFF00-\uFFEF\u0080-\u009F\u2000-\u201f\u2026\u2022\u20ac\r\n])|(\s)/g
    let content = /[@#$%^&*]+/g
    if (emotion.test(param)) {
      return msg = '不支持此类型内容'
    } else if (content.test(param)) {
      return msg = '输入内容有误'
    } else if (!param) {
      return msg = '内容不能为空'
    } else {
      return msg = ''
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})