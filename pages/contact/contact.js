// pages/contact/contact.js
var api = require('../../utils/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.chooseContact({
      success: function(res) {
        console.log(res);
        api.form_('Phone_Recharge/phoneList', {
          userid_locked: wx.getStorageSync('userid_locked'),
          phone: res.phoneNumber
        },
        null,
        function(res) {
          console.log(res.data);
          if (res.data.code == 200) {
            wx.redirectTo({
              url: '../battery/battery?url=' + 'https://api.cd1a.cn/h5/phone/housailei.html'
            });
          }
          
        })
      },
      fail: function(err) {
        console.log(err);
        wx.navigateBack({
          delta: 1
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})