var api = require('../../utils/api.js')
Page({
  data: {
    operator_phone: '',
    data: '',
    height: '',
    guide: wx.getStorageSync('guide'), // 引导
    one: true, // 第一个
    two: false, // 第二个
    three: false, // 第三个
    mask: true,
    location: '',
    enter_coupon: '',
    n: "",
    contentArr: '',
    top: '',
    advertiseTop: '',
    url: '', // 路径
    showCoupon: '', // 电池助力弹框
    phoneNumber: '', //wx.getStorageSync("phoneNumber"), // 用户电话号码
    case: '', // 临时充电
    reson: '', // 月卡充电
    str: '', // 会员
    userid: '', // 邀请新用户
    scanData: '', // 外部扫码数据
    importContent: '',
    popup: ''
  },

  // 下一步
  tap: function () {
    if (this.data.one) {
      this.setData({
        one: false,
        two: true
      })
    } else if (this.data.two) {
      this.setData({
        two: false,
        three: true
      })
    } else if (this.data.three) {
      wx.setStorageSync('guide', 1)
      wx.setStorageSync('hadTouched', 'yes')
      this.setData({
        three: false,
        guide: wx.getStorageSync('guide')
      })
    }
  },

  // 跳转
  jump: function () {
    wx.navigateTo({
      url: '../map/map'
    })
  },

  // 引导图片
  guideOne: function () {
    if (this.data.one) {
      this.setData({
        one: false,
        two: true
      })
    }
  },
  guideTwo: function () {
    if (this.data.two) {
      this.setData({
        two: false,
        three: true
      })
    }
  },
  guideThree: function () {
    if (this.data.three) {
      wx.setStorageSync('guide', 1)
      wx.setStorageSync('hadTouched', 'yes')
      this.setData({
        three: false,
        guide: wx.getStorageSync('guide')
      })
    }
  },

  // 跳过
  center_: function () {
    wx.setStorageSync('guide', 1)
    wx.setStorageSync('hadTouched', 'yes')
    this.setData({
      three: false,
      guide: wx.getStorageSync('guide')
    })
  },

  onShow: function () {
    let that = this
    // 有id获取数据
    that.setData({
      operator_phone: wx.getStorageSync('operator_phone')
    })
    if (wx.getStorageSync('userid_locked')) {
      that.popcop()
    }
  },


  // 6折换电活动 换电
  six_discount: function (url, params = null, paramter = null) {
    let that = this
    let data = {
      qrcode: url,
      userid_locked: wx.getStorageSync("userid_locked") ? wx.getStorageSync("userid_locked") : 0
    }
    wx.setStorageSync('discount', url)
    if (wx.getStorageSync('hadTouched') == 'yes') {
      api.form_('Activity/enter_activity_h', data, null, function success(res) {
        console.log(res)
        if (res.data.result_code == '500') {
          if (params && paramter) {
            let data = {
              userid_locked: wx.getStorageSync('userid_locked'),
              t_id: paramter
            }
            api.active('Trans/records', data, function sucess(res) {
              console.log(res)
              if (res.data.code == 1) {
                // 扫码商城
                wx.redirectTo({
                  url: '/pages/product/product?id=' + params + '&user_id=' + paramter
                })
              } else {
                wx.showModal({
                  content: res.data.msg,
                  showCancel: false
                })
              }
            })
          } else if (params) {
            // 换电
            if (params.match('type')) {
              let param = params.split('type')[0]
              wx.navigateTo({
                url: '../battery/battery?url=' + res.data.data.url + '&hd=' + param + '&type=1'
              })
            } else {
              wx.navigateTo({
                url: '../battery/battery?url=' + res.data.data.url + '&hd=' + params
              })
            }
          } else {
            if (res.data.data.url) {
              // 6折换电
              wx.navigateTo({
                url: '../battery/battery?url=' + res.data.data.url
              })
            } else {
              wx.showModal({
                content: res.data.msg,
                showCancel: false
              })
            }
          }
        } else {
          wx.showModal({
            content: res.data.msg,
            showCancel: false
          })
        }
      })
    } else {
      api.form_('Activity/enter_activity_h', data, null, function success(res) {
        console.log(res)
        if (res.data.result_code == '500') {
          if (params && paramter) {
            let data = {
              userid_locked: wx.getStorageSync('userid_locked'),
              t_id: paramter
            }
            api.active('Trans/records', data, function sucess(res) {
              console.log(res)
              if (res.data.code == 1) {
                wx.setStorageSync('guide', 2)
                // 扫码商城
                wx.redirectTo({
                  url: '/pages/product/product?id=' + params + '&user_id=' + paramter
                })
              } else {
                wx.setStorageSync('guide', 1)
                that.setData({
                  guide: wx.getStorageSync('guide')
                })
                wx.showModal({
                  content: res.data.msg,
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.setStorageSync('guide', 2)
                      that.setData({
                        guide: wx.getStorageSync('guide')
                      })
                    }
                  }
                })
              }
            })
          } else if (params) {
            // 换电
            wx.setStorageSync('guide', 2)
            if (params.match('type')) {
              let param = params.split('type')[0]
              wx.navigateTo({
                url: '../battery/battery?url=' + res.data.data.url + '&hd=' + param + '&type=1'
              })
            } else {
              wx.navigateTo({
                url: '../battery/battery?url=' + res.data.data.url + '&hd=' + params
              })
            }
          } else {
            if (res.data.data.url) {
              wx.setStorageSync('guide', 2)
              // 6折换电
              wx.navigateTo({
                url: '../battery/battery?url=' + res.data.data.url
              })
            } else {
              wx.setStorageSync('guide', 1)
              that.setData({
                guide: wx.getStorageSync('guide')
              })
              wx.showModal({
                content: res.data.msg,
                showCancel: false,
                success: function (res) {
                  if (res.confirm) {
                    wx.setStorageSync('guide', 2)
                    that.setData({
                      guide: wx.getStorageSync('guide')
                    })
                  }
                }
              })
            }
          }
        } else {
          wx.setStorageSync('guide', 1)
          that.setData({
            guide: wx.getStorageSync('guide')
          })
          wx.showModal({
            content: res.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                wx.setStorageSync('guide', 2)
                that.setData({
                  guide: wx.getStorageSync('guide')
                })
              }
            }
          })
        }
      })
    }
  },


  // 邀请购车
  buy_car: function (params) {
    let that = this
    let data = {
      userid_locked: wx.getStorageSync('userid_locked'),
      referrer_id: params
    }
    if (wx.getStorageSync('hadTouched') == 'yes') {
      api.form_('buycar/recommend', data, null, function success(res) {
        console.log(res.data)
        if (res.data.result_code == '373') {
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            success: function () {
              wx.redirectTo({
                url: '/pages/buyCar/procedure/procedure'
              })
            }
          })
        } else {
          wx.showModal({
            content: res.data.msg,
            showCancel: false
          })
        }
      })
    } else {
      api.form_('buycar/recommend', data, null, function success(res) {
        console.log(res.data)
        if (res.data.result_code == '373') {
          wx.setStorageSync('guide', 2)
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            success: function () {
              wx.redirectTo({
                url: '/pages/buyCar/procedure/procedure'
              })
            }
          })
        } else {
          wx.setStorageSync('guide', 1)
          that.setData({
            guide: wx.getStorageSync('guide')
          })
          wx.showModal({
            content: res.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                wx.setStorageSync('guide', 2)
                that.setData({
                  guide: wx.getStorageSync('guide')
                })
              }
            }
          })
        }
      })
    }
  },



  // 扫描设备码
  scan_charge: function () {
    let that = this
    let data = {
      userid_locked: wx.getStorageSync('userid_locked'),
      str: wx.getStorageSync('str')
    }
    if (that.data.enter_coupon.is_old == '2') {
      if (wx.getStorageSync('hadTouched') == 'yes') {
        api.form_('Scan/scan', data, null, function success(res) {
          console.log(res.data)
          switch (res.data.result_code) {
            case "300": //成功
              wx.setStorageSync('operator_phone', res.data.data.operator_phone)
              that.setData({
                scanData: res.data.data,
                operator_phone: res.data.data.operator_phone
              })
              let uri = JSON.stringify(res.data.data)
              let data = encodeURIComponent(uri)
              if (that.data.case) {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true'  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true' 
                  })
                }
                
              }
              if (that.data.reson) {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true'  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true' 
                  })
                }
                
              }
              if (that.data.str) {
                if (that.data.case) {
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true'  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true' 
                    })
                  }
                  
                } else if (that.data.reson) {
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true'  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true' 
                    })
                  }
                  
                } else {
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data 
                    })
                  }
                }
              }
              break;
            default: //其他
              wx.showModal({
                title: '温馨提示',
                content: res.data.msg
              })
              break;
          }
        }, function fail(e) {
          console.log(e)
        })
      } else {
        api.form_('Scan/scan', data, null, function success(res) {
          console.log(res.data)
          switch (res.data.result_code) {
            case "300": //成功
              wx.setStorageSync('guide', 2)
              wx.setStorageSync('operator_phone', res.data.data.operator_phone)
              that.setData({
                scanData: res.data.data,
                operator_phone: res.data.data.operator_phone,
                guide: wx.getStorageSync('guide')
              })
              let uri = JSON.stringify(res.data.data)
              let data = encodeURIComponent(uri)
              if (that.data.case) {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true'  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true' 
                  })
                }
                
              }
              if (that.data.reson) {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true'  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true' 
                  })
                }
                
              }
              if (that.data.str) {
                if (that.data.case) {
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true'  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true' 
                    })
                  }
                  
                } else if (that.data.reson) {
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true'  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true' 
                    })
                  }
                  
                } else {
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data 
                    })
                  }
                  
                }
              }
              break;
            default: //其他
              wx.setStorageSync('guide', 1)
              that.setData({
                guide: wx.getStorageSync('guide')
              })
              wx.showModal({
                title: '温馨提示',
                content: res.data.msg,
                success: function (res) {
                  wx.setStorageSync('guide', 2)
                  if (res.confirm) {
                    that.setData({
                      guide: wx.getStorageSync('guide')
                    })
                  } else {
                    that.setData({
                      guide: wx.getStorageSync('guide')
                    })
                  }
                }
              })
              break;
          }
        }, function fail(e) {
          console.log(e)
        })
      }
    } else {
      api.form_('Scan/scan', data, null, function success(res) {
        that.setData({
          scanData: res.data.data
        })
        console.log(res.data)
        switch (res.data.result_code) {
          case "300": //成功
            wx.setStorageSync('operator_phone', res.data.data.operator_phone)
            that.setData({
              operator_phone: res.data.data.operator_phone
            })
            let uri = JSON.stringify(res.data.data)
            let data = encodeURIComponent(uri)
            if (that.data.case) {
              if (that.data.popup) {
                wx.redirectTo({
                  url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true'  + '&popup=111'
                })
              } else {
                wx.redirectTo({
                  url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true' 
                })
              }
              
            }
            if (that.data.reson) {
              if (that.data.popup) {
                wx.redirectTo({
                  url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true'  + '&popup=111'
                })
              } else {
                wx.redirectTo({
                  url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true' 
                })
              }
              
            }
            if (that.data.str) {
              if (that.data.case) {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true'  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&case=true' 
                  })
                }
                
              } else if (that.data.reson) {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true'  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data + '&reson=true' 
                  })
                }
                
              } else {
                if (that.data.popup) {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data  + '&popup=111'
                  })
                } else {
                  wx.redirectTo({
                    url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data 
                  })
                }
                
              }
            } else {
              if (that.data.popup) {
                wx.redirectTo({
                  url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data  + '&popup=111'
                })
              } else {
                wx.redirectTo({
                  url: '../openCharging/openCharging?support_month_card=' + res.data.data.support_month_card + '&num_id=' + res.data.data.num_id + '&data=' + data 
                })
              }
              
            }
            break;
          default: //其他
            wx.showModal({
              title: '温馨提示',
              content: res.data.msg
            })
            break;
        }
      }, function fail(e) {
        console.log(e)
      })
    }
  },









  // 正则验证
  GetQueryString: function (url, name) {
    let reg = new RegExp("(^|/?|&)" + name + "=([^&]*)(&|$)", "i")
    let r = url.substr(1).match(reg)
    if (r != null) {
      return unescape(r[2])
    };
    return null
  },

  onLoad: function (options) {
    console.log(options);
    let that = this
    let menu = wx.getMenuButtonBoundingClientRect()
    that.setData({
      top: menu.top,
      advertiseTop: menu.top + 30,
    })
    wx.showLoading({
      title: '加载中...'
    })

    if (wx.getStorageSync('hadTouched') == 'yes') {
      wx.setStorageSync('guide', 1)
      that.setData({
        guide: wx.getStorageSync('guide')
      })
    }
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          height: res.screenHeight
        })
      }
    })

    // 首先查看是否为外部扫描二维码进去
    if (options.q != undefined) {
      // 如果存在统一在这里面做处理
      let url = decodeURIComponent(options.q) // 外部扫码
      let userid = that.GetQueryString(url, "userid") //邀请新用户 https://cd1a.cn/pages/index/index?userid=
      let user_id = that.GetQueryString(url, "user_id") //购车邀请好友 https://cd1a.cn/pages/index/index?user_id=
      let six_discount = that.GetQueryString(url, 'six') //6折换电活动的二维码 && 推荐代理
      let s = that.GetQueryString(url, 's') // 设备
      console.log(url)
      wx.setStorageSync('scanCode', 2)
      wx.setStorageSync('scan_code', 111)
      if (!wx.getStorageSync('userid_locked')) {
        wx.setStorageSync('guide', 2)
        that.setData({
          guide: wx.getStorageSync('guide')
        })
        wx.login({
          success: function (result) {
            let data

            if (userid) {
              data = {
                code: result.code,
                recommend: userid
              }
            } else {
              data = {
                code: result.code
              }
            }

            wx.showLoading({
              title: '加载中...',
              mask: true
            })

            api.get_('Login/index', data, null, function success(res) {
                console.log(res.data)
                if (res.data.result_code == '360') {
                  wx.setStorageSync('userid_locked', res.data.data.userid_locked)
                  wx.setStorageSync('phoneNumber', res.data.data.phone)
                  that.setData({
                    phoneNumber: res.data.data.phone
                  })

                  if (s) {
                    if (url.indexOf('?') == '-1') {
                      wx.setStorageSync('str', url)
                    } else {
                      if (url.split('?')[1].split('=')[0] == 's') {
                        wx.setStorageSync('str', url)
                      }
                    }
                  }
                  that.popcop()
                  
                  setTimeout(() => {
                    wx.hideLoading()
                    // 设备
                    if (s) {
                      if (url.indexOf('?') == '-1') {
                        wx.setStorageSync('str', url)
                        that.scan_charge()
                      } else {
                        if (url.split('?')[1].split('=')[0] == 's') {
                          wx.setStorageSync('str', url)
                          that.scan_charge()
                        } else {
                          that.six_discount(url)
                          setTimeout(() => {
                            that.popcop()
                          }, 1000)
                        }
                      }
                    }

                    // 购车邀请好友
                    if (user_id) {
                      that.buy_car(user_id)
                    }

                    // 6折换电活动 && 推荐代理 && 扫码商城
                    if (six_discount) {
                      let a_id = that.GetQueryString(url, 'a_id')
                      let goods_id = that.GetQueryString(url, 'goods_id')
                      let users_id = that.GetQueryString(url, 'users_id')
                      let hd = that.GetQueryString(url, 'hd')
                      if (a_id) {
                        that.six_discount(url, a_id) // 推荐代理
                      } else if (goods_id && users_id) {
                        that.six_discount(url, goods_id, users_id) // 扫码商城
                      } else if (hd) {
                        that.six_discount(url, hd) // 换电 (邀请好友助力)
                      } else {
                        that.six_discount(url) // 6折换电 换电
                      }
                    }

                    // 其他的外部扫码没有这些判断的都走设备扫码
                    if (!user_id && !userid && !six_discount && !s) {
                      if (url.indexOf('?') == '-1') {
                        wx.setStorageSync('str', url)
                        that.scan_charge()
                      } else {
                        // fn 粉丝团
                        that.six_discount(url)
                        setTimeout(() => {
                          that.popcop()
                        }, 1000)
                      }
                    }
                  }, 1000)
                } else {
                  wx.showModal({
                    content: res.data.msg,
                    showCancel: false
                  })
                }
              },
              function fail(e) {
                console.log(e)
              })
          }
        })
      } else {
        wx.showLoading({
          title: '加载中...',
          mask: true
        })

        if (s) {
          if (url.indexOf('?') == '-1') {
            wx.setStorageSync('str', url)
          } else {
            if (url.split('?')[1].split('=')[0] == 's') {
              wx.setStorageSync('str', url)
            }
          }
        }
        that.popcop()

        setTimeout(() => {
          // 6折换电活动 && 推荐代理 && 扫码商城
          wx.hideLoading()

          if (six_discount) {
            let a_id = that.GetQueryString(url, 'a_id')
            let goods_id = that.GetQueryString(url, 'goods_id')
            let users_id = that.GetQueryString(url, 'users_id')
            let hd = that.GetQueryString(url, 'hd')
            if (a_id) {
              that.six_discount(url, a_id) // 推荐代理
            } else if (goods_id && users_id) {
              that.six_discount(url, goods_id, users_id) // 扫码商城
            } else if (hd) {
              that.six_discount(url, hd) // 换电 (邀请好友助力)
            } else {
              that.six_discount(url) // 6折换电 换电
            }
          }

          // 邀请新用户
          if (userid) {
            that.new_friend(userid)
          }

          // 设备
          if (s) {
            if (url.indexOf('?') == '-1') {
              wx.setStorageSync('str', url)
              that.scan_charge()
            } else {
              if (url.split('?')[1].split('=')[0] == 's') {
                wx.setStorageSync('str', url)
                that.scan_charge()
              } else {
                that.six_discount(url)
              }
            }
          }

          // 购车邀请好友
          if (user_id) {
            that.buy_car(user_id)
          }

          // 其他的外部扫码没有这些判断的都走设备扫码
          if (!user_id && !userid && !six_discount && !s) {
            if (url.indexOf('?') == '-1') {
              wx.setStorageSync('str', url)
              that.scan_charge()
            } else {
              // fn 粉丝团
              that.six_discount(url)
            }
          }
        }, 1000)
      }
    } else {
      // 没有用户id
      if (!wx.getStorageSync('userid_locked')) {
        wx.setStorageSync('guide', 2)
        that.setData({
          guide: wx.getStorageSync('guide')
        })
        wx.login({
          success: function (result) {
            let data = {
              code: result.code
            }
            api.get_('Login/index', data, null, function success(res) {
                console.log(res.data)
                if (res.data.result_code == '360') {
                  wx.setStorageSync('userid_locked', res.data.data.userid_locked)
                  wx.setStorageSync('phoneNumber', res.data.data.phone)
                  that.setData({
                    phoneNumber: res.data.data.phone
                  })
                  that.popcop()
                } else {
                  wx.showModal({
                    content: res.data.msg,
                    showCancel: false
                  })
                }
              },
              function fail(e) {
                console.log(e)
              })
          }
        })
      }

      // 其他页面传递过来的参数
      if (options.case) { //临时充电
        that.setData({
          case: options.case
        })
        that.scan_charge()
      }
      if (options.reson) { //月卡充电
        that.setData({
          reson: options.reson
        })
        that.scan_charge()
      }
      if (options.popup) {
        that.setData({
          popup: options.popup
        })
        that.scan_charge()
      }
      if (options.str) { // 购买月卡，会员卡，优惠券 之类的做重新跳转
        that.setData({
          str: options.str
        })
        that.scan_charge()
      }
      if (options.user_id) { // 小程序分享购车页面
        that.buy_car(options.user_id)
      }
      if (options.userid) { // 小程序分享邀请新用户
        that.new_friend(options.userid)
      }

      if (options.scanData) { // 扫码数据
        let data = decodeURIComponent(options.scanData)
        let newData = JSON.parse(data)
        that.setData({
          scanData: newData
        })
      }
    }
  },

  go: function () {
    wx.navigateTo({
      // url: '../battery/battery?url=https://api.cd1a.cn/activity/dist/save_img.html',
      url: '../battery/battery?url=https://api.cd1a.cn/activity/v3/try_drive.html',
      // url: '../battery/battery?url=https://api.cd1a.cn/activity/dist/try_drive.html',
      // url: '../battery/battery?url=https://api.cd1a.cn/activity/hd4/index.html'
    })
  },

  //老版本远程开启
  remoteOpen: function () {
    if (!wx.getStorageSync('phoneNumber')) {
      if (this.data.scanData) {
        let uri = JSON.stringify(this.data.scanData)
        let data = encodeURIComponent(uri)
        wx.redirectTo({
          url: '../authorize/authorize?scanData=' + data
        })
      } else {
        wx.redirectTo({
          url: '../authorize/authorize'
        })
      }
    } else {
      wx.navigateTo({
        url: '../remoteOpen/remoteOpen'
      })
    }
  },

  // 内部扫码
  scanCode: function () {
    if (!wx.getStorageSync('phoneNumber')) {
      if (this.data.scanData) {
        let uri = JSON.stringify(this.data.scanData)
        let data = encodeURIComponent(uri)
        wx.redirectTo({
          url: '../authorize/authorize?scanData=' + data
        })
      } else {
        wx.redirectTo({
          url: '../authorize/authorize'
        })
      }
    } else {
      if (this.data.enter_coupon.is_new == '2') {
        if (this.data.enter_coupon.skip == '1') {
          if (this.data.scanData) {
            let uri = JSON.stringify(this.data.scanData)
            let data = encodeURIComponent(uri)
            if (this.data.popup) {
              wx.redirectTo({
                url: '../openCharging/openCharging?support_month_card=' + this.data.scanData.support_month_card + '&num_id=' + this.data.scanData.num_id + '&data=' + data  + '&popup=111'
              })
            } else {
              wx.redirectTo({
                url: '../openCharging/openCharging?support_month_card=' + this.data.scanData.support_month_card + '&num_id=' + this.data.scanData.num_id + '&data=' + data 
              })
            }
            
          } else {
            wx.navigateTo({
              url: '../remoteOpen/remoteOpen'
            })
          }
        } else if (this.data.enter_coupon.skip == '2') {
          wx.navigateTo({
            url: '../countDown/countDown?log_id=' + this.data.enter_coupon.user_log_id + '&get_coupon=' + 1
          })
        } else {
          wx.navigateTo({
            url: '../charge/charge'
          })
        }
      } else {
        this.sweepCode()
      }
    }
  },

  // 换电
  battery: function () {
    wx.navigateTo({
      url: '../battery/battery?url=' + this.data.enter_coupon.battery_button_url
    })
  },

  //弹窗
  popcop() {
    let that = this
    let location
    let n = 0;
    let data = {
      "userid_locked": wx.getStorageSync('userid_locked'), //用户唯一识别
      "str": wx.getStorageSync('str') ? wx.getStorageSync('str') : 0
    }
    api.form_('Money/enter_coupon', data, null, function success(res) {
      console.log(res.data.data)
      let contentArr = []
      if (res.data.data) {
        wx.setStorageSync('open_tell', res.data.data.open_tell)
        wx.hideLoading()
        
        if (res.data.data.location.length > 15) {
          location = res.data.data.location.substr(0, 15)
        }
        
        let listNum =  res.data.data.list.length;

        if (listNum > 4) {
          n += 1
          that.setData({
            importContent: res.data.data.list.slice(0, 4),
            n
          })
        } else {
          that.setData({
            importContent: res.data.data.list
          })
        }

        for (let i = that.data.n * 4; i < res.data.data.list.length; i++) {
          contentArr.push(res.data.data.list[i])
        }
        if (res.data.data.battery_help == '2') {
          that.setData({
            showCoupon: res.data.data.battery_help, // 电池助力弹框
            guide: false
          })
        }
        that.setData({
          location: location,
          contentArr: contentArr,
          enter_coupon: res.data.data,
          n,
        })
      }
    }, function fail(e) {
      wx.showToast({
        title: '网络连接失败,请检查网络',
        icon: 'none'
      })
    })
  },

  close: function () {
    this.setData({
      showCoupon: 1, // 电池助力弹框
      guide: true
    })
  },

  //用户协议
  agreement: function () {
    wx.navigateTo({
      url: '../agreement/agreement'
    })
  },

  // 客服电话
  mobie: function () {
    wx.makePhoneCall({
      phoneNumber: '4006888919'
    })
  },

  // 运维电话
  phone: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.operator_phone
    })
  },

  // 签到
  signin: function () {
    wx.navigateTo({
      url: '../signin/signin'
    })
  },

  // 游戏
  gameLogo: function () {
    wx.navigateTo({
      url: '../gameLogo/gameLogo?url=' + this.data.enter_coupon.ad_url,
    })
  },

  // 个人中心获取用户头像
  getUserProfile: function (e) {
    let that = this;
    wx.showLoading({
      title: '请稍等',
      mask: true
    })
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        wx.login({
          success: function (result) {
            let data = {
              encryptedData: res.encryptedData,
              iv: res.iv,
              rawData: res.rawData,
              code: result.code
            }
            api.get_('Login/index', data, null, function success(res) {
                console.log(res.data)
                if (res.data.result_code == '360') {
                  wx.setStorageSync('userid_locked', res.data.data.userid_locked)
                  wx.setStorageSync('phoneNumber', res.data.data.phone)
                  that.setData({
                    phoneNumber: res.data.data.phone
                  })
                  wx.redirectTo({
                    url: '../personalCenter/personalCenter'
                  })
                } else {
                  wx.showModal({
                    content: res.data.msg,
                    showCancel: false
                  })
                }
              },
              function fail(e) {
                console.log(e)
              })
          }
        })
      },
      fail: (err) => {
        console.log(err)
        wx.showToast({
          title: '登录失败',
          icon: 'none'
        })
      }
    })
  },

  // 跳转个人中心
  getPersonalCenter: function () {
    wx.navigateTo({
      url: '../personalCenter/personalCenter'
    })
  },

  // 自动获取电话并且授权并跳转个人中心
  getPhoneNumber: function (e) {
    let that = this
    // 点击确定按钮
    if (e.detail.errMsg == 'getPhoneNumber:ok') {
      let data = {
        type: 1,
        userid_locked: wx.getStorageSync('userid_locked')
      }
      api.form_('Money/judge_phone', data, null, function success(res) {
        console.log(res.data)
        // 371已绑定手机号 370未绑定手机号
        if (res.data.result_code == '370') {
          wx.login({
            success: function (res) {
              let data = {
                encryptedData: e.detail.encryptedData,
                iv: e.detail.iv,
                code: res['code']
              }
              console.log(data)
              api.get_('Login/get_applet_phone', data, null, function success(res) {
                console.log(res)
                if (res.data.result_code == '361') {
                  wx.setStorageSync("phoneNumber", res.data.phone)
                  that.setData({
                    phoneNumber: res.data.phone
                  })
                  wx.showToast({
                    title: '获取成功',
                    icon: 'success',
                    complete: function () {
                      wx.navigateTo({
                        url: '../personalCenter/personalCenter'
                      })
                    }
                  })
                } else {
                  wx.showModal({
                    content: res.data.msg,
                    showCancel: false
                  })
                }
              }, function fail(e) {
                console.log(e)
              })
            }
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            success: function () {
              if (res.data.data.phone) {
                wx.setStorageSync('phoneNumber', res.data.data.phone)
                that.setData({
                  phoneNumber: wx.getStorageSync("phoneNumber")
                })
                wx.navigateTo({
                  url: '../personalCenter/personalCenter'
                })
              }
            }
          })
        }
      })
    } else {
      // 点击取消按钮
      wx.showModal({
        content: '授权失败',
        showCancel: false
      })
    }
  },

  // 小程序内部扫码
  sweepCode: function () {
    let that = this
    wx.scanCode({
      success: (res) => {
        console.log(res.result)
        let url = res.result // 外部扫码
        let userid = that.GetQueryString(url, "userid") //邀请新用户 https://cd1a.cn/pages/index/index?userid=
        let user_id = that.GetQueryString(url, "user_id") //购车邀请好友 https://cd1a.cn/pages/index/index?user_id=
        let six_discount = that.GetQueryString(url, 'six') //6折换电活动的二维码 && 推荐代理
        let s = that.GetQueryString(url, 's') // 设备

        // 6折换电活动 && 推荐代理 && 扫码商城
        if (six_discount) {
          let a_id = that.GetQueryString(url, 'a_id')
          let goods_id = that.GetQueryString(url, 'goods_id')
          let users_id = that.GetQueryString(url, 'users_id')
          let hd = that.GetQueryString(url, 'hd')
          if (a_id) {
            that.six_discount(url, a_id) // 推荐代理
          } else if (goods_id && users_id) {
            that.six_discount(url, goods_id, users_id) // 扫码商城
          } else if (hd) {
            that.six_discount(url, hd) // 换电 (邀请好友助力)
          } else {
            that.six_discount(url) // 6折换电 换电
          }
        }
        // 邀请新用户
        if (userid) {
          that.new_friend(userid)
        }

        // 购车邀请好友
        if (user_id) {
          that.buy_car(user_id)
        }

        // 设备
        if (s) {
          if (url.indexOf('?') == '-1') {
            wx.setStorageSync('str', url)

            let data = {
              userid_locked: wx.getStorageSync('userid_locked'),
              str: wx.getStorageSync('str')
            }
            api.form_('Scan/scan', data, null, function success(res) {
              console.log(res.data)
              switch (res.data.result_code) {
                case "300": //成功
                  wx.setStorageSync('scan_code', 111)
                  let uri = JSON.stringify(res.data.data)
                  let data = encodeURIComponent(uri)
                  wx.setStorageSync('operator_phone', res.data.data.operator_phone)
                  that.setData({
                    operator_phone: res.data.data.operator_phone
                  })
                  if (that.data.popup) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.support_month_card + '&num_id=' + res.data.num_id + '&data=' + data  + '&popup=111'
                    })
                  } else {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + res.data.support_month_card + '&num_id=' + res.data.num_id + '&data=' + data
                    })
                  }
                  
                  break;
                default: //其他
                  wx.showModal({
                    title: '温馨提示',
                    content: res.data.msg
                  })
                  break;
              }
            }, function fail(e) {
              console.log(e)
            })
          } else {
            if (url.split('?')[1].split('=')[0] == 's') {
              wx.setStorageSync('str', url)

              let data = {
                userid_locked: wx.getStorageSync('userid_locked'),
                str: wx.getStorageSync('str')
              }
              api.form_('Scan/scan', data, null, function success(res) {
                console.log(res.data)
                switch (res.data.result_code) {
                  case "300": //成功
                    wx.setStorageSync('scan_code', 111)
                    let uri = JSON.stringify(res.data.data)
                    let data = encodeURIComponent(uri)
                    wx.setStorageSync('operator_phone', res.data.data.operator_phone)
                    that.setData({
                      operator_phone: res.data.data.operator_phone
                    })
                    if (that.data.popup) {
                      wx.redirectTo({
                        url: '../openCharging/openCharging?support_month_card=' + res.data.support_month_card + '&num_id=' + res.data.num_id + '&data=' + data + '&popup=111'
                      })
                    } else {
                      wx.redirectTo({
                        url: '../openCharging/openCharging?support_month_card=' + res.data.support_month_card + '&num_id=' + res.data.num_id + '&data=' + data
                      })
                    }
                    
                    break;
                  default: //其他
                    wx.showModal({
                      title: '温馨提示',
                      content: res.data.msg
                    })
                    break;
                }
              }, function fail(e) {
                console.log(e)
              })
            } else {
              that.six_discount(url)
            }
          }
        }

        // 其他的外部扫码没有这些判断的都走设备扫码
        if (!user_id && !userid && !six_discount && !s) {
          if (url.indexOf('?') == '-1') {
            wx.setStorageSync('str', url)
            that.scan_charge()
          } else {
            // fn 粉丝团
            that.six_discount(url)
          }
        }
      },
      fail: (res) => {
        console.log('失败')
      }
    })
  },

  // 新版本内容跳转
  getContent: function (e) {
    let index = e.currentTarget.dataset.index
    let data = this.data.enter_coupon.list[index]
    let content = '1'
    if (index == 0) {
      if (!wx.getStorageSync('phoneNumber')) {
        if (this.data.scanData) {
          let uri = JSON.stringify(this.data.scanData)
          let data = encodeURIComponent(uri)
          wx.redirectTo({
            url: '../authorize/authorize?scanData=' + data
          })
        } else {
          wx.redirectTo({
            url: '../authorize/authorize'
          })
        }
      } else {
        this.showContent(content, data)
      }
    } else {
      this.showContent(content, data)
    }
  },

  getNewtent: function (e) {
    let index = e.currentTarget.dataset.index
    let data = this.data.contentArr[index]
    let content = '1'
    this.showContent(content, data)
  },

  // 轮播
  carouselTap: function (e) {
    let index = e.currentTarget.dataset.index
    let data = this.data.enter_coupon.carousel[index]
    let content = '2'
    this.showContent(content, undefined, data)
  },

  // 内容展示通用方法
  showContent: function (param, objFirst = null, objSecond = null) {
    // console.log(param, objFirst, objSecond);
    // param 1 内容 2 轮播
    if (param == '1') {
      const reg = objFirst.url.search("appKey");
      // 开启充电 1 换电 2 商城 3 合伙人 4
      if (objFirst.type == '1') {
        // 可参与
        if (objFirst.is_enter == '2') {
          if (this.data.enter_coupon.is_new == '2') {
            // 是否本地连接 1 不是 2 是
            if (objFirst.is_self == '2') {
              if (this.data.scanData) {
                let uri = JSON.stringify(this.data.scanData)
                let data = encodeURIComponent(uri)
                if (this.data.popup) {
                  if (reg == -1) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + this.data.scanData.support_month_card + '&num_id=' + this.data.scanData.num_id + '&data=' + data  + '&popup=111'
                    })
                  } else {
                    wx.setStorageSync('appKey', objFirst.url)
                    wx.navigateTo({
                      url: '../activity/activity'
                    })
                  }
                } else {
                  if (reg == -1) {
                    wx.redirectTo({
                      url: '../openCharging/openCharging?support_month_card=' + this.data.scanData.support_month_card + '&num_id=' + this.data.scanData.num_id + '&data=' + data 
                    })
                  } else {
                    wx.setStorageSync('appKey', objFirst.url)
                    wx.navigateTo({
                      url: '../activity/activity'
                    })
                  }
                }
                
              } else {
                wx.navigateTo({
                  url: objFirst.self_url
                })
              }
            } else {
              if (reg == -1) {
                wx.navigateTo({
                  url: '../battery/battery?url=' + objFirst.url
                })
              } else {
                wx.setStorageSync('appKey', objFirst.url)
                wx.navigateTo({
                  url: '../activity/activity'
                })
              }
            }
          } else {
            this.sweepCode()
          }
        } else {
          // 不可参与
          wx.showModal({
            content: objFirst.msg,
            showCancel: false
          })
        }
      } else if (objFirst.type == '2') {
        // 可参与
        if (objFirst.is_enter == '2') {
          // 是否本地连接 1 不是 2 是
          if (objFirst.is_self == '2') {
            wx.navigateTo({
              url: objFirst.self_url
            })
          } else {
            if (objFirst.car_shop_appid) {
              wx.navigateToMiniProgram({
                appId: objFirst.car_shop_appid,
                path: objFirst.url_platform,
                success: function (res) {
                  let data = {
                    userid_locked: wx.getStorageSync('userid_locked'),
                    url: objFirst.url_platform
                  }
                  api.form_('Statist/out_applet', data, null, function success(res) {
                    console.log(res)
                  })
                },
                extraData: {
                  foo: 'bar'
                },
                envVersion: 'release'
              })
            } else {
              if (reg == -1) {
                wx.navigateTo({
                  url: '../battery/battery?url=' + objFirst.url
                })
              } else {
                wx.setStorageSync('appKey', objFirst.url)
                wx.navigateTo({
                  url: '../activity/activity'
                })
              }
            }
          }
        } else {
          // 不可参与
          wx.showModal({
            content: objFirst.msg,
            showCancel: false
          })
        }
      }
    } else {
      const reg = objSecond.url.search("appKey");
      // 1 内部链接 2 外部链接 
      if (objSecond.type == '1') {
        if (objSecond.is_enter == '2') {
          if (objSecond.is_self == '2') {
            wx.navigateTo({
              url: objSecond.self_url,
            })
          } else {
            if (reg == -1) {
              wx.navigateTo({
                url: '../battery/battery?url=' + objSecond.url
              })
            } else {
              wx.setStorageSync('appKey', objSecond.url)
              wx.navigateTo({
                url: '../activity/activity'
              })
            }
          }
        } else {
           // 不可参与
           wx.showModal({
            content: objSecond.msg,
            showCancel: false
          })
        }
        
      } else if (objSecond.type == '2') {
        if (objSecond.is_enter == '2') {
          if (objSecond.is_self == '2') {
            wx.navigateTo({
              url: objSecond.self_url,
            })
          } else {
            if (objSecond.car_shop_appid) {
              wx.navigateToMiniProgram({
                appId: objSecond.car_shop_appid,
                path: objSecond.url_platform,
                success: function (res) {
                  let data = {
                    userid_locked: wx.getStorageSync('userid_locked'),
                    url: objSecond.url_platform
                  }
                  api.form_('Statist/out_applet', data, null, function success(res) {
                    console.log(res)
                  })
                },
                extraData: {
                  foo: 'bar'
                },
                envVersion: 'release'
              })
            } else {
              if (reg == -1) {
                wx.navigateTo({
                  url: '../battery/battery?url=' + objSecond.url
                })
              } else {
                wx.setStorageSync('appKey', objSecond.url)
                wx.navigateTo({
                  url: '../activity/activity'
                })
              }
            }
            
          }
        } else {
           // 不可参与
           wx.showModal({
            content: objSecond.msg,
            showCancel: false
          })
        }
      }
    }
  }
})