var api = require('../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    icon1: true,
    icon2: false,
    index: 2,
    data: [],
    isSaveing: true,
    open_tell: '',
    id:'',
    case: '',
    reson: '',
    head: '',
    last: '',
    popup: ''
  },
  onLoad: function (options) {
    let that = this
    that.setData({
      open_tell: wx.getStorageSync('open_tell')
    })
    var str = wx.getStorageSync('str')
    if(str) {
      this.setData({
        head: str.split('?')[0],
        last: str.split('?')[1]
      })
    }
    if (options.id) {
      that.setData({
        id: options.id
      })
    }
    if (options.case) {
      that.setData({
        case: options.case
      })
    }
    if (options.reson) {
      that.setData({
        reson: options.reson
      })
    }
    if (options.popup) {
      this.setData({
        popup: options.popup
      })
    }
    // 会员卡信息
    let data = {
      scan: options.sacn,
      switch_vip: options.switch_,
      vip_sale: options.vip_sale,
      userid_locked: wx.getStorageSync("userid_locked")
    }
    api.form_('Card/vip_detail', data, null, function success(res) {
      that.setData({
        data: res.data.data
      })
    }, function fail(e) {
      console.log(e)
    })

  },
  icon1: function (e) {
    this.setData({
      icon1: false,
      icon2: true,
      index: e.currentTarget.dataset.index
    })
  },
  icon2: function (e) {
    this.setData({
      icon1: true,
      icon2: false,
      index: e.currentTarget.dataset.index
    })
  },
  // 确认开通
  memberbtn: function (e) {
    let that = this.data;
    let this_ = this;
    // if (this_.data.isSaveing)
    //   return;
    //   this_.setData({
    //   isSaveing: true
    // })
    wx.createSelectorQuery().select('.all').boundingClientRect(function (rect) {
      // 使页面滚动到底部
      wx.pageScrollTo({
        scrollTop: rect.bottom
      })
    }).exec()
    wx.showToast({
      title: '提交中...',
      icon: 'loading'
    })
    if (this_.data.index == 2) {
      wx.showToast({
        title: '请勾选权益',
        icon: 'loading'
      })
    } else {
      if (this_.data.isSaveing) {
        this_.setData({
          isSaveing: false
        })
        wx.login({
          success: function (res) {
            let data = {
              code: res['code'],
              fee: that.data.member_money,
              userid_locked: wx.getStorageSync('userid_locked'),
              member: 2
            }
            api.form_('pay/pay_fee', data, null, function success(res) {
              if (res.data.result_code == 314) {
                let order_id = res.data.data.order_id;
                if (order_id == "") {
                  wx.showToast({
                    title: '请重新提交',
                    icon: 'loading'
                  })
                  this.setData({
                    isSaveing: true
                  })
                } else {
                  wx.requestPayment({
                    timeStamp: res.data.data.timeStamp,
                    nonceStr: res.data.data.nonceStr,
                    package: res.data.data.package,
                    signType: 'MD5',
                    paySign: res.data.data.paySign,
                    success: function (res) {
                      let data = {
                        order_id: order_id,
                        userid_locked: wx.getStorageSync('userid_locked')
                      }
                      api.form_('pay/check_pay', data, null, function success(res) {
                        this_.setData({
                          isSaveing: true
                        })
                        console.log(res)
                        if (that.id == 1) {
                          wx.redirectTo({
                            url: '../cardBag/cardBag'
                          })
                        } else {
                          if (this_.data.case) {
                            if (this_.data.popup) {
                              wx.reLaunch({
                                url: '../index/index?str=' + this_.data.head + `&${this_.data.last}` + '&case=true' + '&popup=111'
                              })
                            } else {
                              wx.reLaunch({
                                url: '../index/index?str=' + this_.data.head + `&${this_.data.last}` + '&case=true'
                              })
                            }
                            
                          } else if(this_.data.reson) {
                            if (this_.data.popup) {
                              wx.reLaunch({
                                url: '../index/index?str=' + this_.data.head + `&${this_.data.last}` + '&reson=true' + '&popup=111'
                              })
                            } else {
                              wx.reLaunch({
                                url: '../index/index?str=' + this_.data.head + `&${this_.data.last}` + '&reson=true'
                              })
                            }
                            
                          } else {
                            if (this_.data.popup) {
                              wx.reLaunch({
                                url: '../index/index?str=' + that.data.head + `&${that.data.last}` + '&popup=111'
                              })
                            } else {
                              wx.reLaunch({
                                url: '../index/index?str=' + that.data.head + `&${that.data.last}`
                              })
                            }
                            
                          }
                        }
                      }, function fail(e) {
                        this_.setData({
                          isSaveing: true
                        })
                        console.log(e)
                      })
                    },
                    fail: function (e) {
                      wx.showToast({
                        title: '支付失败...',
                        icon: 'loading'
                      })
                      this_.setData({
                        isSaveing: true
                      })
                    }
                  })
                }
              } else {
                this_.setData({
                  isSaveing: true
                })
                wx.showToast({
                  title: res.data.msg,
                  icon: 'none'
                })
              }
            }, function fail(e) {
              this_.setData({
                isSaveing: true
              })
              console.log(e)
            })
          }
        })
      }
    }
  },
  // 会员卡权益
  vip: function () {
    wx.navigateTo({
      url: '../vipEquity/vipEquity?card_id=' + this.data.data.card_id
    })
  }
})