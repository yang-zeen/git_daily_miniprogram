// pages/payment/payment.js
var api = require('../../utils/api')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money: "",
    rule: "",
    type: "",
    refund: "",
    order: "",
    message: "", // 其他
    orderId: "",
    touch: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if (options.type) {
      this.setData({
        type: options.type
      })
    }
    if (options.orderId) {
      this.setData({
        orderId: options.orderId
      })
    }
    if (options.refund) {
      this.setData({
        refund: options.refund
      })
      api.form_('Serve_car/refund_detail', {
          userid_locked: wx.getStorageSync('userid_locked'),
          appoint_id: options.orderId
        },
        null,
        res => {
          console.log(res.data)
          this.setData({
            money: res.data.list.money / 100,
            order: res.data.list.order_id,
            rule: res.data.list.tips.replace(/(^\n+)|(\n+$)|\n+/g, "<br />")
          })
        },
        err => {
          console.log(err)
        })
    } else {
      console.log(this.data.refund)
      api.form_('Serve_car/pay_rule', {
          userid_locked: wx.getStorageSync('userid_locked'),
          type: options.type == '1' ? options.type : '2',
          apponit_id: options.orderId
        },
        null,
        res => {
          console.log(res.data)
          this.setData({
            money: res.data.data.money / 100,
            rule: res.data.data.des.replace(/(^\n+)|(\n+$)|\n+/g, "<br />")
          })
        },
        err => {
          console.log(err)
        }
      )
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  regRule(param) {
    let msg
    let emotion = /([^\u0020-\u007E\u00A0-\u00BE\u2E80-\uA4CF\uF900-\uFAFF\uFE30-\uFE4F\uFF00-\uFFEF\u0080-\u009F\u2000-\u201f\u2026\u2022\u20ac\r\n])|(\s)/g
    let content = /[@#$%^&*]+/g
    if (emotion.test(param)) {
      return msg = '禁止输入表情符/颜文字等非法字符'
    } else if (content.test(param)) {
      return msg = '输入内容有误'
    } else if (!param) {
      return msg = '内容不能为空'
    } else {
      return msg = ''
    }
  },

  submit() {
    let that = this
    if (that.data.touch) {
      that.setData({
        touch: false
      })
      wx.login({
        success: function (res) {
          let data = {
            code: res['code'],
            fee: that.data.money * 100,
            member: that.data.type == '1' ? 19 : 20,
            appoint_id: that.data.orderId,
            userid_locked: wx.getStorageSync('userid_locked'),
          }
          api.form_('pay/pay_fee', data, null, function (res) {
            console.log(res.data)
            if (res.data.result_code == 314) {
              let order_id = res.data.data.order_id
              if (order_id == "") {
                wx.showToast({
                  title: '请重新提交',
                  icon: 'loading'
                })
              } else {
                wx.requestPayment({
                  timeStamp: res.data.data.timeStamp,
                  nonceStr: res.data.data.nonceStr,
                  package: res.data.data.package,
                  signType: 'MD5',
                  paySign: res.data.data.paySign,
                  success(res) {
                    let data = {
                      order_id: order_id,
                      userid_locked: wx.getStorageSync('userid_locked')
                    }
                    api.form_('pay/check_pay', data, null, function success(res) {
                      console.log(res)
                      if (res.data.result_code == '300') {
                        wx.showToast({
                          title: res.data.msg,
                          icon: 'loading',
                          duration: 2000
                        })
                        setTimeout(() => {
                          wx.reLaunch({
                            url: '../home/home',
                          })
                        }, 1000)
                      } else if (res.data.result_code == '342') {
                        wx.showToast({
                          title: res.data.msg,
                          icon: 'loading',
                          duration: 2000
                        })
                        setTimeout(() => {
                          wx.reLaunch({
                            url: '../home/home',
                          })
                        }, 1000)
                      } else {
                        wx.showToast({
                          title: res.data.msg,
                          icon: 'loading',
                          duration: 2000
                        })
                        setTimeout(() => {
                          wx.reLaunch({
                            url: '../home/home',
                          })
                        }, 1000)
                      }
                    })
                  },
                  fail(res) {
                    wx.showToast({
                      title: '支付失败',
                      icon: 'none',
                      duration: 2000
                    })
                  }
                })
              }
            } else {
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              })
            }
          })
        }
      })
      setTimeout(() => {
        that.setData({
          touch: true
        })
      }, 4000)
    } else {
      wx.showToast({
        title: '您点击的太快, 请稍后再试',
        icon: 'none'
      })
    }
  },

  refund() {
    if (this.regRule(this.data.message)) {
      wx.showToast({
        title: this.regRule(this.data.message),
        icon: 'none'
      })
    } else {
      if (this.data.touch) {
        this.setData({
          touch: false
        })
        api.form_('Serve_car/refund_submit', {
          userid_locked: wx.getStorageSync('userid_locked'), 
          appoint_id: this.data.orderId,
          order_id: this.data.order,
          money: this.data.money * 100,
          remark: this.data.message
        },
        null,
        res => {
          console.log(res.data)
          if (res.data.result_code == "204") {
            wx.showToast({
              title: res.data.msg
            })
            setTimeout(() => {
              wx.reLaunch({
                url: '../home/home',
              })
            }, 1000)
          } else {
            wx.showToast({
              title: res.data.msg,
              icon: "none"
            })
          }
        },
        fail => {
          console.log(fail)
        })
        setTimeout(() => {
          this.setData({
            touch: true
          })
        }, 4000)
      } else {
        wx.showToast({
          title: '您点击的太快, 请稍后再试',
          icon: 'none'
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})