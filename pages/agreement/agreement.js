var api = require('../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data: '',
    open_tell: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      open_tell: wx.getStorageSync('open_tell') 
    })
    api.form_('Agreement/statement', {}, null, (res) => {
      this.setData({
        data: res.data.data.agreement
      })
    }, (e) => {
      console.log(e)
    })

  }
})