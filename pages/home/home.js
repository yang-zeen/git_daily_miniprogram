// pages/home/home.js
var api = require('../../utils/api.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    resData: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  go(e) {
    let index = e.currentTarget.dataset.index
    if (this.data.resData.arr[index].is_enter == '2') {
      if (this.data.resData.arr[index].is_self == '1') {
        wx.navigateTo({
          url: this.data.resData.arr[index].self_url + '?type=' + Number(index + 1)
        }) 
      } else {
        wx.navigateTo({
          url: '../battery/battery?url=' + this.data.resData.arr[index].url
        })
      }
    } else {
      wx.showToast({
        title: this.data.resData.arr[index].msg,
        icon: 'none'
      })
    }
  },

  jump(e) {
    let obj = this.data.resData.foot[e.currentTarget.dataset.index]
    this.getUrl(obj)
  },

  // swiperImg(e) {
  //   let obj = this.data.resData.li[e.currentTarget.dataset.index]
  //   this.getUrl(obj)
  // },

  getUrl(obj) {
    if (obj.is_enter == '2') {
      if (obj.is_self == '1') {
        wx.navigateTo({
          url: obj.self_url
        })
      } else {
        wx.navigateTo({
          url: '../battery/battery?url=' + obj.url
        })
      }
    } else {
      wx.showToast({
        title: obj.msg,
        icon: 'none'
      })
    }
  },

  enter() {
    this.getUrl(this.data.resData.order)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    api.form_('Serve_car/index', {
        userid_locked: wx.getStorageSync('userid_locked')
      },
      null,
      function success(res) {
        console.log(res.data.data)
        that.setData({
          resData: res.data.data
        })
      },
      function fail(err) {
        console.log(err)
      }
    )
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})