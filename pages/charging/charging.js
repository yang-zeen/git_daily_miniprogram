// pages/charging/charging.js
var api  = require("../../utils/api");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 0,
    fee: 0,
    date: "",
    value: "",
    recordDate: "",
    checked: false,
    result: [],
    arr: [],
    idArr: [],
    l: false,
    r: false,
    bottom: [
      {
        title: "",
        value: "one"
      },
      {
        title: "全部全选",
        value: "two"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    if (month < 10) {
      month = "0" + month;
    }
    this.setData({
      date: year + "年" + month + "月"
    });

    let formdate = year + '-' + month
    const res = await this.resData("Invoice/index", {
      userid_locked: wx.getStorageSync("userid_locked"),
      time: formdate
    });
    if (res.result_code == 1000) {
      if (res.data.list.length) {
        for (let i = 0; i < res.data.list.length; i++) {
          this.data.arr.push(i);
        }

        this.setData({
          recordDate: res.data.list,
          arr: this.data.arr
        });

        return;
      }
    } else {
      wx.showToast({
        icon: "none",
        title: res.msg,
      });
    }
  },

  resData: function(url, data) {
    return new Promise((resolve, reject) => {
      api.form_(
      url,
      data,
      undefined,
      function success(res) {
        resolve(res.data)
      },
      function err(err) {
        reject(err)
      }
      );
    })
  },

  bindDateChange: async function(e) {
    let value = e.detail.value
    let year = value.split("-")[0] + "年" + value.split("-")[1] + "月";
    let res = await this.resData("Invoice/index", {
      userid_locked: wx.getStorageSync("userid_locked"),
      time: value
    });
    console.log(res);
    if (res.result_code == 1000) {
      if (res.data.list.length) {
        this.setData({
          result: [],
          arr: [],
          idArr: [],
          recordDate: "",
          date: year
        });

        for (let i = 0; i < res.data.list.length; i++) {
          this.data.arr.push(i);
        }

        this.setData({
          recordDate: res.data.list,
          arr: this.data.arr
        });

        return;
      }
    } else {
      wx.showToast({
        icon: "none",
        title: res.msg,
      });
    }
  },

  step: function() {
    if (this.data.recordDate.length == 0 || !this.data.recordDate) return wx.showToast({
      title: '暂无可使用列表',
      icon: "none"
    })
    let params = {};
    if (this.data.idArr.length != 0) {
      params.ids = String(this.data.idArr);
      params.nums = this.data.num;
      params.fee = this.data.fee;
      wx.navigateTo({
        url: '../form/form?id=' + encodeURIComponent(JSON.stringify(params))
      });
    } else {
      wx.showToast({
        title: '请选择时间',
        icon: "none"
      })
    }

    if (this.data.r) {
      params.nums = this.data.num;
      params.fee = this.data.fee;
      wx.navigateTo({
        url: '../form/form?id=' + encodeURIComponent(JSON.stringify(params))
      });
    }
  },

  onChange: function(e) {
    // console.log(e.detail);
    this.setData({
      idArr: []
    })
    let sum = 0
    if (e.detail.length > 0) {
      for (let i = 0; i < e.detail.length; i++) {
        let index = e.detail[i];
        sum += this.data.recordDate[index].consume/1;
        this.data.idArr.push(this.data.recordDate[index].id);
      }
    } else {
      this.setData({
        l: false,
        r: false
      })
    }

    this.setData({
      result: e.detail,
      num: e.detail.length,
      fee: sum,
      idArr: this.data.idArr
    });
  },

  left: function (e) {
    this.setData({
      idArr: []
    });

    if (!this.data.l) {
      let sum = 0;
      let arr = this.data.arr.map(item => item.toString());
      for (let i = 0; i < this.data.arr.length; i++) {
        sum += this.data.recordDate[i].consume/1;
        this.data.idArr.push(this.data.recordDate[i].id)
      }
      this.setData({
        result: arr,
        num: this.data.arr.length,
        fee: sum,
        idArr: this.data.idArr,
        l: !this.data.l,
        r: false
      })

      return;
    }
    
    this.setData({
      result: [],
      num: 0,
      fee: 0,
      idArr: [],
      l: !this.data.l,
      r: false
    })
  },

  right: async function(e) {
    this.setData({
      idArr: []
    });
    if (!this.data.r) {
      let arr = this.data.arr.map(item => item.toString());
      let res = await this.resData("Invoice/totalSelect", {
        userid_locked: wx.getStorageSync("userid_locked")
        });
      console.log(res);
      if (res.result_code == 1000) {
        this.setData({
          result: arr,
          num: res.data.total_num,
          fee: res.data.total_money,
          l: false,
          r: !this.data.r
        })
      } else {
        wx.showToast({
          title: res.msg,
          icon: "none"
        })
      }

      return;
    }
    
    this.setData({
      result: [],
      num: 0,
      fee: 0,
      l: false,
      r: !this.data.r
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})