var api = require('../../utils/api.js')
Page({
  data: {
    data: '',
    id:'',
    isJoin: '',
    reson: '',
    case: '',
    url: '',
    popup: '',
    open_tell: wx.getStorageSync('open_tell')
  },

  onShow: function() {
    let that = this
    let pages = getCurrentPages()
    if(pages.length > 1) {
      let prePage = pages[pages.length - 2]
      prePage.setData({
        changeData:  that.data.isJoin
      })
    }
  },

  onLoad: function (options) {
    let that = this
    that.setData({
      open_tell: wx.getStorageSync('open_tell')
    })
    if (options.id) {
      that.setData({
        id: options.id
      })
    }
    if (options.reson) {
      this.setData({
        reson: options.reson
      })
    }
    if (options.case) {
      this.setData({
        case: options.case
      })
    }
    if (options.popup) {
      this.setData({
        popup: options.popup
      })
    }
    let data = {
      userid_locked: wx.getStorageSync("userid_locked")
    }
    api.form_('Money/active_list', data, null, function success(res) {
      console.log(res.data)
      that.setData({
        data: res.data.data,
      })
      if(res.data.data) {
        res.data.data.forEach(item => {
          console.log(item)
          if(item.url_type == '4') {
            that.setData({
              isJoin: item.is_join,
            })
          } 
          if(item.url_type == 5) {
            that.setData({
              url: item.url,
            })
          }
        })
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    }, function fail(e) {
      wx.showToast({
        title: '网络连接失败,请检查网络',
        icon: 'none'
      })
    })
  },
  
  img_jump(o) {
    let type = o.currentTarget.dataset.type;
    if (type == 2) {
      //会员
      if(this.data.case) {
        if (this.data.popup) {
          wx.navigateTo({
            url: '../member/member?id=' +  this.data.id + '&case=true&popup=111'
          })
        } else {
          wx.navigateTo({
            url: '../member/member?id=' +  this.data.id + '&case=true'
          })
        }
      } else if(this.data.reson) {
        if (this.data.popup) {
          wx.navigateTo({
            url: '../member/member?id=' +  this.data.id + '&reson=true&popup=111'
          })
        } else {
          wx.navigateTo({
            url: '../member/member?id=' +  this.data.id + '&reson=true'
          })
        }
      } else {
        if (this.data.popup) {
          wx.navigateTo({
            url: '../member/member?id=' +  this.data.id + '&popup=111'
          })
        } else {
          wx.navigateTo({
            url: '../member/member?id=' +  this.data.id
          }) 
        }
      }
    } else if (type == 3) {
      //月卡
      if(this.data.reson) { // 临时
        // console.log(this.data.reson)
        if (this.data.popup) {
          wx.navigateTo({
            url: '../mcard/mcard?id=' + this.data.id + '&reson=true&popup=111'
          })
        } else {
          wx.navigateTo({
            url: '../mcard/mcard?id=' + this.data.id + '&reson=true'
          })
        }
        
      } else if(this.data.case) { // 月卡
        // console.log(this.data.case)
        if (this.data.popup) {
          wx.navigateTo({
            url: '../mcard/mcard?id=' + this.data.id + '&case=true&popup=111'
          })

        } else {
          wx.navigateTo({
            url: '../mcard/mcard?id=' + this.data.id + '&case=true'
          })
        }
      } else {
        if (this.data.popup) {
          wx.navigateTo({
            url: '../mcard/mcard?id=' + this.data.id + '&popup=111'
          })
        } else {
          wx.navigateTo({
            url: '../mcard/mcard?id=' + this.data.id
          })
        }
      }
    } else if (type == 4) {
      if(this.data.isJoin == '2') {
        wx.navigateTo({
          url: '../buyCar/rate/rate',
        })
      } else {
        //购车
        wx.navigateTo({
          url: '../buyCar/invitation/invitation'
        })
      }
    } else if (type == 5) {
      wx.navigateTo({
        url: '../battery/battery?url=' + this.data.url
      })
    }
  }
})